### Lambda Parser

This Haskell module, `LambdaParser`, provides parsers for various expressions in the lambda calculus, including logical expressions, arithmetic expressions, comparisons, and list operations. Below is a breakdown of the functionalities provided by this module:

#### Part 1: Lambda Calculus Expressions

**Exercise 1: Parsing Lambda Calculus Expressions**

- `longLambdaP`: Parses a string representing a lambda calculus expression in long form.
- `shortLambdaP`: Parses a string representing a lambda calculus expression in short form.
- `lambdaP`: Parses a string representing a lambda calculus expression in either short or long form.

#### Part 2: Logical and Arithmetic Expressions

**Exercise 1: Parsing Logical Expressions**

- `logicP`: Parses a logical expression and returns it in lambda calculus.

**Exercise 2: Parsing Arithmetic Expressions**

- `basicArithmeticP`: Parses simple arithmetic expressions involving addition and subtraction with natural numbers into lambda calculus.
- `arithmeticP`: Parses arithmetic expressions involving addition, subtraction, multiplication, exponentiation, and parentheses with natural numbers into lambda calculus.

**Exercise 3: Parsing Comparison Operations**

- `complexCalcP`: Parses arithmetic expressions involving comparison operations (<=, !=) with natural numbers into lambda calculus.

#### Part 3: List Operations

**Exercise 1: Parsing List Operations**

- `listP`: Parses a string representing a list in lambda calculus.
- `listOpP`: Parses list operations such as `head`, `rest`, and `isNull` on lambda calculus lists.

### Usage

To use this module, import it into your Haskell project. You can then utilize the provided parser functions to parse lambda calculus expressions, logical expressions, arithmetic expressions, comparison operations, and list operations. Ensure that you provide input strings conforming to the expected formats specified in the documentation for each parser function.

For example:
```haskell
-- Parse a lambda calculus expression in short form
result <- parse lambdaP "λx.xx"

-- Parse a logical expression
result <- parse logicP "True and False"

-- Parse a complex arithmetic expression
result <- parse arithmeticP "5 + 9 * 3 - 2**3"
```

Ensure that you handle the parsing results appropriately based on your application's requirements.

For more detailed usage examples and information, refer to the documentation provided for each parser function in the module.
